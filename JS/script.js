let tabsTitles = document.querySelectorAll('.tabs-title')
let tabsContent = document.querySelectorAll('.hidden')
let tabsName

for(let i = 0; i < tabsTitles.length; i++) {
    tabsTitles[i].addEventListener("click", selectTabsTitle);
}

function selectTabsTitle() {
    for(let i = 0; i < tabsTitles.length; i++) {
        tabsTitles[i].classList.remove('tabs-title-active');
    }
    this.classList.add('tabs-title-active')
    tabsName = this.getAttribute('data-name')
    selectTabContent(tabsName)
}

function selectTabContent(tabName) {
    for(let i = 0; i < tabsContent.length; i++) {
        if (tabsContent[i].classList.contains(tabName)){
            tabsContent[i].classList.add('active')
        } else{
            tabsContent[i].classList.remove('active')
        }
    }
}

let loadBtn = document.querySelector('.loading-button')
let imgDiv = document.querySelector('.work-tabs-title-content')
let loadAnim = document.querySelector('.loading-animation')

loadBtn.addEventListener('click', (e) =>{
    e.preventDefault()
    loadAnim.classList.remove('loading-animation')
    loadBtn.replaceWith(loadAnim)
    setTimeout(function () {
        imgDiv.classList.remove('all-overflow')
        loadAnim.remove()
    }, 2000)
})

let imgTabsTitles = document.querySelectorAll('.amazing-work-tabs-title')
let imgTabsContent = document.querySelectorAll('.image-container')
let imgTabsName

for(let i = 0; i < imgTabsTitles.length; i++) {
    imgTabsTitles[i].addEventListener("click", selectImgTabsTitle);
}

function selectImgTabsTitle() {
    for(let i = 0; i <  imgTabsTitles.length; i++) {
        imgTabsTitles[i].classList.remove('amazing-work-tabs-title-active');
    }
    this.classList.add('amazing-work-tabs-title-active')
    imgTabsName = this.getAttribute('data-name')
    selectImgTabContent(imgTabsName)
    if (imgTabsName !== "all"){
        loadBtn.classList.add('hidden-img')
    } else{
        loadBtn.classList.remove('hidden-img')
    }
}

function selectImgTabContent(imgTabName) {
    for(let i = 0; i <  imgTabsContent.length; i++) {
        if ( imgTabsContent[i].classList.contains(imgTabName)){
            imgTabsContent[i].classList.remove("hidden-img")
        } else{
            imgTabsContent[i].classList.add("hidden-img")
        }
    }

}

// cлайдер

new Glide('.glide',{
    type: 'carousel'
}).mount()